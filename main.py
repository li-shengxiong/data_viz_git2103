#下面就是字符串写HTML页面的方式，一句话一句的拼接，然后再实现
import random

from flask import Flask,redirect
app=Flask(__name__)
@app.route('/')
def show_index():
    return  redirect('/static/index.html')

#API - Application Programming Interface 应用程序编程接口
#网络API（网络数据接口） - 请求这个URL（同一资源定位器）就能获得相应的数据（通常是JSON格式）
@app.route('/api/fruits')
def get_fruits():
    fruits_list = [
        '苹果','香蕉','山竹','榴莲','杨梅','草莓','蓝莓','石榴','番茄',
        '杨桃','哈密瓜','西瓜','葡萄','百香果','樱桃','车厘子','荔枝'
    ]
    fruits_count = random.randrange(3,6)
    selected_fruits = random.sample(fruits_list,fruits_count)
    return {'fruits':selected_fruits}#flask会将python中的字典处理成JSON
    #return  render_template('index.html',selected__fruits=selected_fruits)


if __name__=='__main__':
    app.run(host='172.26.134.165',debug=True)

